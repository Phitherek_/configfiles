-- Conky settings --
conky.config = {
    background=true,
    update_interval=1,

    cpu_avg_samples=2,
    net_avg_samples=2,

    override_utf8_locale=true,

    double_buffer=true,
    no_buffers=true,

    text_buffer_size=2048,
    -- imlib_cache_size 0,

    -- temperature_unit fahrenheit,

    -- Window specifications --

    own_window=true,
    own_window_type='override',
    own_window_transparent=true,
    --own_window_argb_visual=true,
    own_window_hints='undecorated,sticky,skip_taskbar,skip_pager,below',

    border_inner_margin=0,
    border_outer_margin=0,

    minimum_width=250,
    minimum_height=500,
    maximum_width=1000,

    alignment='top_right',
    gap_x=35,
    gap_y=35,

    -- Graphics settings --
    draw_shades=false,
    draw_outline=false,
    draw_borders=false,
    draw_graph_borders=false,

    -- Text settings --
    use_xft=true,
    font='TerminessTTF Nerd Font:size=9',
    xftalpha=0.5,

    uppercase=false,

    temperature_unit='celsius',


    default_color='FFFFFF',
    lua_load='~/.conky_lua_scripts.lua'
};

conky.text = [[
    ${voffset 8}${color 737373}${font TerminessTTF Nerd Font:size=15}${time %A}${font}${voffset -8}${alignr 50}${color FFFFFF}${font TerminessTTF Nerd Font:size=38}${time %e}${font}
    ${color FFFFFF}${voffset -30}${color FFFFFF}${font TerminessTTF Nerd Font:size=18}${time %b}${font}${voffset -3} ${color FFFFFF}${font TerminessTTF Nerd Font:size=21}${time %Y}${font}${color FFFFFF}${hr}
    ${font TerminessTTF Nerd Font:size=38}${alignr}${time %H:%M:%S}
    ${font TerminessTTF Nerd Font:size=13.5}${voffset -30}${alignr}${time %s} 
    ${voffset 75}${font TerminessTTF Nerd Font:size=12}${alignr}$USER${font}
    ${font TerminessTTF Nerd Font:size=12}
    ${color 737373} AC adapter ${if_match "${acpiacadapter ACAD}" == "on-line"}${color green}${else}${color red}${endif} ${acpiacadapter ACAD}
    ${color 737373} Battery ${if_match ${battery_percent BAT1}>10}${if_match ${battery_percent BAT1}>30}${color green}${else}${color yellow}${endif}${else}${color red}${endif} ${battery BAT1} ${battery_time BAT1} ${battery_bar BAT1 8,225}

     ${if_match ${cpu cpu1}<90}${if_match ${cpu cpu1}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu1}}%      ${if_match ${cpu cpu2}<90}${if_match ${cpu cpu2}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu2}}%      ${if_match ${cpu cpu3}<90}${if_match ${cpu cpu3}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu3}}%      ${if_match ${cpu cpu4}<90}${if_match ${cpu cpu4}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu4}}%      ${if_match ${cpu cpu5}<90}${if_match ${cpu cpu5}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu5}}%      ${if_match ${cpu cpu6}<90}${if_match ${cpu cpu6}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu6}}%      ${if_match ${cpu cpu7}<90}${if_match ${cpu cpu7}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu7}}%      ${if_match ${cpu cpu8}<90}${if_match ${cpu cpu8}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu8}}%      ${if_match ${cpu cpu9}<90}${if_match ${cpu cpu9}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu9}}%       ${if_match ${cpu cpu10}<90}${if_match ${cpu cpu10}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu10}}%       ${if_match ${cpu cpu11}<90}${if_match ${cpu cpu11}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu11}}%       ${if_match ${cpu cpu12}<90}${if_match ${cpu cpu12}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${lua_parse pad_percentage ${cpu cpu12}}%
    ${color 737373} CPU1      CPU2      CPU3      CPU4      CPU5      CPU6      CPU7      CPU8      CPU9      CPU10      CPU11      CPU12

    ${color 737373} RAM ${if_match ${memperc}<90}${if_match ${memperc}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${memperc}% ${membar 8,225}
    ${color 737373} Swap ${if_match ${swapperc}<90}${if_match ${swapperc}<70}${color green}${else}${color yellow}${endif}${else}${color red}${endif}${swapperc}% ${swapbar 8,225}

    ${color 737373} Average load ${color white}${loadavg}
    ${color 737373} Users ${color white} ${user_number} 

    ${color green}${voffset 23}${downspeed enp65s0}
    ${color red}${upspeed enp65s0}
    ${color 737373}  Ethernet
    ${color green}${voffset 25}${downspeed wlp0s20f3}
    ${color red}${upspeed wlp0s20f3}
    ${color 737373}  WLAN

    ${color 737373}Lan ${if_match "${addr enp65s0}" == "No Address"}${color red}${else}${color green}${endif}${addr enp65s0}
    ${color 737373}WiFi ${if_match "${addr wlp0s20f3}" == "No Address"}${color red}${else}${color green}${endif}${addr wlp0s20f3} 
    ${color 737373}WiFi (cont'd) ${if_match "${wireless_essid wlp0s20f3}" == "off/any"}${color red}${else}${if_match "${wireless_link_qual_perc wlp0s20f3}" == "unk"}${color red}${else}${if_match ${wireless_link_qual_perc wlp0s20f3}>60}${color green}${else}${if_match ${wireless_link_qual_perc wlp0s20f3}>30}${color yellow}${else}${color red}${endif}${endif}${endif}${endif}${wireless_essid wlp0s20f3} at ${wireless_link_qual_perc wlp0s20f3}% ${alignr}${wireless_link_bar 8,225 wlp0s20f3}

    ${color 737373}Root ${if_match ${fs_free_perc}<10}${if_match ${fs_free_perc}<5}${color red}${else}${color yellow}${endif}${else}${color green}${endif} ${alignc} ${fs_bar 8,225 /}  ${alignr}${fs_free /}${color white}

    ${color 737373}Media player (${color white}${exec smart-playerctl current-player}${color 737373}) ${if_match "${exec smart-playerctl status}" == "Playing"}${color green}${else}${if_match "${exec smart-playerctl status}" == "Paused"}${color yellow}${else}${color red}${endif}${endif} ${exec smart-playerctl status}
    ${color 737373}Now playing ${color white} ${exec smart-playerctl metadata xesam:title} by ${exec smart-playerctl metadata xesam:artist} from ${exec smart-playerctl metadata xesam:album}

${font TerminessTTF Nerd Font:size=10}

    ${color 737373}Hostname:${color white} ${nodename}
    ${color 737373}Kernel:${color white} ${kernel}
    ${color 737373}Machine:${color white} ${machine}
    ${color 737373}Uptime:${color white} ${uptime}
]]
