function conky_pad_percentage(number)
    return string.format( '%3i' , conky_parse( number ) )
end
